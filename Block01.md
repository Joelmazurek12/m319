# Mein Portofolio

## Block 1
---
### **Was ist mein Ziel für heute?** 
#### *Neues rund ums Programmieren zu lernen.*
---

### **Was habe ich bis jetzt gelernt?** 
#### *Wie man eine Markdown datei erstellt und runter lädt. Ebenfalls habe ich gelernt wie man aus Visual Studio Code eine PDF macht mithilfe von Markdown.*
---

### **Was bereitet mir noch schwierigkeiten?**
#### *Bis jetzt gibt es noch keine schwierigkeiten.* 
---

### **Wie löse ich diese schwierigkeit?** 
#### *Gibt keine.*
---


## Block 2
---
### **Was ist mein Ziel für heute?** 
#### *Alle Diagramme zu lernen & verstehen*
---

### **Was habe ich bis jetzt gelernt?** 
#### *Alle Diagramme verstanden*
---

### **Was bereitet mir noch schwierigkeiten?**
#### *Konzentration*
---

### **Wie löse ich diese schwierigkeit?**
#### *Geht im Sommer nicht*